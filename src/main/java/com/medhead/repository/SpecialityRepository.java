package com.medhead.repository;

import com.medhead.model.Speciality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {
    @Query("SELECT CASE WHEN COUNT(s) > 0 THEN TRUE ELSE FALSE END FROM speciality s WHERE s.specName = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT s FROM speciality s WHERE s.specName = ?1")
    Optional<Speciality> findByName(String name);
}
