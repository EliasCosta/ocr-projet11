package com.medhead.repository;

import com.medhead.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    @Query(value = "SELECT p FROM Patient p WHERE p.name LIKE %?1%")
    Patient findByName(String name);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN TRUE ELSE FALSE END FROM Patient p WHERE p.name = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT p.name FROM Patient p WHERE p.name LIKE ?1")
    Optional<Patient> findByNewName(String name);

}
