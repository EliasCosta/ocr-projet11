package com.medhead.repository;

import com.medhead.model.Hospital;
import com.medhead.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Integer> {

    @Query(value = "SELECT h FROM Hospital h WHERE h.name LIKE %?1%")
    Hospital findByName(String name);

    @Query(value = "SELECT h.name FROM Hospital h JOIN h.speciality s WHERE s.specName LIKE %?1%")
    List<String> findBySpeciality(String speciality);

    @Query("SELECT CASE WHEN COUNT(h) > 0 THEN TRUE ELSE FALSE END FROM Hospital h WHERE h.name = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT h.name FROM Hospital h WHERE h.name LIKE ?1")
    Optional<Hospital> findByNewName(String name);

}