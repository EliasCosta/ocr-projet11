package com.medhead.repository;

import com.medhead.model.GroupSpec;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<GroupSpec, Integer> {
    @Query("SELECT CASE WHEN COUNT(g) > 0 THEN TRUE ELSE FALSE END FROM GroupSpec g WHERE g.groupName = ?1 ")
    Boolean selectExistsName(String name);

    @Query("SELECT g FROM GroupSpec g WHERE g.groupName = ?1")
    Optional<GroupSpec> findByName(String name);
}
