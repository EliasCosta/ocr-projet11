package com.medhead.controller;

import com.medhead.model.Hospital;
import com.medhead.service.HospitalService;
import com.medhead.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GetNearestHospital {

    @Autowired
    private PatientService patientService;

    @Autowired
    private HospitalService hospitalService;

    @GetMapping("/patientaddress/{name}")
    public ArrayList<Double> getPatientCoordinates(@PathVariable(value = "name") String name){
        ArrayList<Double> patientCoordinates = new ArrayList<Double>( );
        double patientLatitude = patientService.getPatientLatitudeByName(name);
        patientCoordinates.add(patientLatitude);
        double patientLongitude = patientService.getPatientLongitudeName(name);
        patientCoordinates.add(patientLongitude);
        return patientCoordinates;
    }

    @GetMapping("/hospitaladdress/{name}")
    public List<Double> getHospitalCoordinates(@PathVariable(value = "name") String name) {
        List<Double> hospitalCoordinates = new ArrayList<Double>();
        double hospitalLatitude = hospitalService.getHospitalLatitudeByName(name);
        hospitalCoordinates.add(hospitalLatitude);
        double hospitalLongitude = hospitalService.getHospitalLongitudeName(name);
        hospitalCoordinates.add(hospitalLongitude);
        return hospitalCoordinates;
    }

    @GetMapping("/nearesthospital/{name}/{speciality}")
    public String getNearestHospital(@PathVariable(value = "name") String name, @PathVariable(value = "speciality") String speciality ) {
        List<Double> hospitalCoordinates = new ArrayList<Double>();
        List<Double> patientCoordinates = new ArrayList<Double>( );
        patientCoordinates = getPatientCoordinates(name);
        Double nearestDistance = null;
        String nearestHospital = null;

        List<String> hospitals = hospitalService.findHospitalBySpeciality(speciality);
        for (String hospital: hospitals) {
            hospitalCoordinates= getHospitalCoordinates(hospital);
            Double distance = distance(hospitalCoordinates.get(0),patientCoordinates.get(0), hospitalCoordinates.get(1),patientCoordinates.get(1));
            if (nearestDistance == null || distance < nearestDistance){
                nearestDistance = distance;
                nearestHospital = hospital;
            }
        }
        return "L'hôpital " + nearestHospital + " est l'hôpital le plus proche situé à " + nearestDistance +" km de " + name;
    }


    public static double distance(double lat1, double lat2, double lon1, double lon2) {

        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = 6371;

        // calculate the result
        return(c * r);
    }

}
