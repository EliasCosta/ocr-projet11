package com.medhead.controller;

import com.medhead.model.Patient;
import com.medhead.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping
    public List<Patient> getAllPatients(){
        return patientService.getAllPatients();
    }

    @GetMapping("/{name}")
    public Patient getPatientByName(@PathVariable String name){
        return patientService.findPatientByName(name);
    }

    @PostMapping
    public ResponseEntity<Patient> savePatient(@RequestBody Patient patient){
        return new ResponseEntity<Patient>(patientService.savePatient(patient), HttpStatus.CREATED);
    }

    @DeleteMapping
    public void deletePatient(@RequestBody Patient patient) {
        patientService.deletePatient(patient);
    }

    @PutMapping
    public ResponseEntity<Patient> updatePatient(@RequestBody Patient patient) {
        return new ResponseEntity<Patient>(patientService.updatePatient(patient), HttpStatus.OK);
    }

}
