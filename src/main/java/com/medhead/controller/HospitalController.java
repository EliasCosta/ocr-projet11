package com.medhead.controller;

import com.medhead.model.Hospital;
import com.medhead.model.Patient;
import com.medhead.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hospital")
public class HospitalController {

    @Autowired
    private HospitalService hospitalService;

    @GetMapping
    public List<Hospital> getAllHospitals(){
        return hospitalService.getAllHospitals();
    }

    @PostMapping
    public ResponseEntity<Hospital> saveHospital(@RequestBody Hospital hospital){
        return new ResponseEntity<Hospital>(hospitalService.saveHospital(hospital), HttpStatus.CREATED);
    }

    @GetMapping("/{name}")
    public Hospital getHospitalByName(@PathVariable String name){
        return hospitalService.findHospitalByName(name);
    }

    @DeleteMapping
    public void deleteHospital(@RequestBody Hospital hospital) {
        hospitalService.deleteHospital(hospital);
    }

    @PutMapping
    public ResponseEntity<Hospital> updateHospital(@RequestBody Hospital hospital) {
        return new ResponseEntity<Hospital>(hospitalService.updateHospital(hospital), HttpStatus.OK);
    }

    @GetMapping("/v/{speciality}")
    public List<String> getHospitalBySpeciality(@PathVariable String speciality){
        return hospitalService.findHospitalBySpeciality(speciality);
    }

}
