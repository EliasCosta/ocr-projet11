package com.medhead.controller;
import com.medhead.model.GroupSpec;
import com.medhead.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @GetMapping
    public List<GroupSpec> getAllGroupOfSpecialities(){
        return groupService.getAllGroups();
    }

    @PostMapping
    public ResponseEntity<GroupSpec> saveGroupOfSpeciality(@RequestBody GroupSpec group){
        return new ResponseEntity<GroupSpec>(groupService.saveGroup(group), HttpStatus.CREATED);
    }

    @DeleteMapping
    public void deleteGroupSpec(@RequestBody GroupSpec group){
        groupService.deleteGroup(group);
    }

    @PutMapping
    public ResponseEntity<GroupSpec> updateGroupOfSpeciality(@RequestBody GroupSpec group){
        return new ResponseEntity<GroupSpec>(groupService.updateGroup(group), HttpStatus.OK);
    }
}
