package com.medhead.service;

import com.medhead.model.GroupSpec;

import java.util.List;

public interface GroupService {
    List<GroupSpec> getAllGroups();
    GroupSpec saveGroup(GroupSpec group);
    void deleteGroup(GroupSpec group);
    GroupSpec updateGroup(GroupSpec group);
}
