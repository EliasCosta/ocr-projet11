package com.medhead.service;

import com.medhead.model.GroupSpec;
import com.medhead.repository.GroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService{

    @Autowired
    private GroupRepository groupRepository;

    @Override
    public List<GroupSpec> getAllGroups() {
        return groupRepository.findAll();
    }

    @Override
    public GroupSpec saveGroup(GroupSpec group) {
        return groupRepository.save(group);
    }

    @Override
    public void deleteGroup(GroupSpec group) {
        groupRepository.delete(group);
    }

    @Override
    public GroupSpec updateGroup(GroupSpec group) {
        return groupRepository.save(group);
    }
}
