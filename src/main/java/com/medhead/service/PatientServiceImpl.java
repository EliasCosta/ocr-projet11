package com.medhead.service;

import com.medhead.model.Patient;
import com.medhead.model.Speciality;
import com.medhead.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService{

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    @Override
    public Patient savePatient(Patient patient) {
        return patientRepository.save(patient);
    }

    @Override
    public Patient findPatientByName(String name) {
        return patientRepository.findByName(name);
    }


    @Override
    public Double getPatientLatitudeByName(String name) {

        Patient patient = new Patient();
        try{
            patient = patientRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return patient.getLatitude();
    }

    @Override
    public Double getPatientLongitudeName(String name) {

        Patient patient = new Patient();
        try{
            patient = patientRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return patient.getLongitude();
    }

    @Override
    public void deletePatient(Patient patient) {
        patientRepository.delete(patient);
    }

    @Override
    public Patient updatePatient(Patient patient) {
        return patientRepository.save(patient);
    }
}
