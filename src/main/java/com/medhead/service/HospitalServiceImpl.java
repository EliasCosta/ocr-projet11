package com.medhead.service;

import com.medhead.model.Hospital;
import com.medhead.repository.HospitalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalServiceImpl implements HospitalService{

    @Autowired
    private HospitalRepository hospitalRepository;

    @Override
    public List<Hospital> getAllHospitals() {
       return hospitalRepository.findAll();
    }

    @Override
    public Hospital saveHospital(Hospital hospital) {
        return hospitalRepository.save(hospital);
    }

    @Override
    public Hospital findHospitalByName(String name) {
        return hospitalRepository.findByName(name);
    }

    @Override
    public Double getHospitalLatitudeByName(String name) {
        Hospital hospital = new Hospital();
        try{
            hospital = hospitalRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return hospital.getLatitude();
    }

    @Override
    public Double getHospitalLongitudeName(String name) {
        Hospital hospital = new Hospital();
        try{
            hospital = hospitalRepository.findByName(name);
        } catch (Exception e){
            e.printStackTrace();
        }
        return hospital.getLongitude();
    }

    @Override
    public void deleteHospital(Hospital hospital) {
        hospitalRepository.delete(hospital);
    }

    @Override
    public Hospital updateHospital(Hospital hospital) {
        return hospitalRepository.save(hospital);
    }

    @Override
    public List<String> findHospitalBySpeciality(String speciality) {
        List<String> hospital = hospitalRepository.findBySpeciality(speciality);
        return hospital;
    }
}
