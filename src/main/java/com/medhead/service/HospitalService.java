package com.medhead.service;

import com.medhead.model.Hospital;

import java.util.List;

public interface HospitalService {
    List<Hospital> getAllHospitals();
    Hospital saveHospital(Hospital hospital);
    Hospital findHospitalByName(String name);
    Double getHospitalLatitudeByName(String name);
    Double getHospitalLongitudeName(String name);
    void deleteHospital(Hospital hospital);
    Hospital updateHospital(Hospital hospital);
    List<String> findHospitalBySpeciality(String speciality);
}
