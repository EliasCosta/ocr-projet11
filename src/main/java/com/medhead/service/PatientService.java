package com.medhead.service;

import com.medhead.model.Patient;

import java.util.List;

public interface PatientService {
    List<Patient> getAllPatients();
    Patient savePatient(Patient patient);
    Patient findPatientByName(String name);
    Double getPatientLatitudeByName(String name);
    Double getPatientLongitudeName(String name);
    void deletePatient(Patient patient);
    Patient updatePatient(Patient patient);
}
