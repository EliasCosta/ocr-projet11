package com.medhead.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity(name = "speciality")
@Table(name = "speciality")
public class Speciality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "spec_name")
    private String specName;

    @ManyToOne
    @JoinColumn(name = "group_id")
    private GroupSpec group;

}
