package com.medhead.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private double latitude;

    private double longitude;

    @OneToMany
    @JoinColumn(name = "specialityP_id")
    private List<Speciality> speciality = new ArrayList<>();

}