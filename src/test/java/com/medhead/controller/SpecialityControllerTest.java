package com.medhead.controller;

import com.medhead.service.GroupService;
import com.medhead.service.SpecialityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@WebMvcTest(controllers = SpecialityController.class)
class SpecialityControllerTest {

    @MockBean
    private GroupService groupService;

    @MockBean
    private SpecialityService specialityService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetSpecialities() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/speciality")).andExpect(MockMvcResultMatchers.status().isOk());
    }

}