package com.medhead.controller;

import com.medhead.service.GroupService;
import com.medhead.service.HospitalService;
import com.medhead.service.PatientService;
import com.medhead.service.SpecialityService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@WebMvcTest(controllers = GetNearestHospital.class)
class GetNearestHospitalTest {

    @MockBean
    private GroupService groupService;

    @MockBean
    private SpecialityService specialityService;

    @MockBean
    private HospitalService hospitalService;

    @MockBean
    private PatientService patientService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetNearestHospital() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/nearesthospital/{name}/{speciality}", "Claire Marie", "Endodontie"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}