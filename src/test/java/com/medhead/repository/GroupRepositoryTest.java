package com.medhead.repository;

import com.medhead.model.GroupSpec;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;


@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class GroupRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;

    // JUnit test for saveGroup

    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveGroupSpecTest(){

        GroupSpec groupSpec = GroupSpec.builder().groupName("Fetish").build();

        groupRepository.save(groupSpec);

        boolean expected = groupRepository.selectExistsName("Fetish");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(2)
    public void getGroupSpecTest(){

        GroupSpec groupSpec = groupRepository.findById(1).get();

        Assertions.assertThat(groupSpec.getId()).isEqualTo(1);
    }


    @Test
    @Order(3)
    public void getListOfGroupSpecTest(){

        List<GroupSpec> groupSpec = groupRepository.findAll();

        Assertions.assertThat(groupSpec.size()).isGreaterThan(0);

    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updateGroupSpecTest(){

        GroupSpec groupSpec = groupRepository.findById(1).get();

        groupSpec.setGroupName("Macumba");

        GroupSpec groupSpecUpdated = groupRepository.save(groupSpec);

        Assertions.assertThat(groupSpecUpdated.getGroupName()).isEqualTo("Macumba");
    }


   /* @Test
    @Order(5)
    @Rollback(value = false)
    public void deleteGroupSpecTest(){

        GroupSpec groupSpec = groupRepository.findById(1).get();

        groupRepository.delete(groupSpec);

        GroupSpec groupSpec1 = null;

        Optional<GroupSpec> optionalGroupSpec = groupRepository.findByName("Macumba");

        if (optionalGroupSpec.isPresent()){
            groupSpec1 = optionalGroupSpec.get();
        }

        Assertions.assertThat(groupSpec1).isNull();
    }*/
}