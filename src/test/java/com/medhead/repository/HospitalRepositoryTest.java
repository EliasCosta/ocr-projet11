package com.medhead.repository;


import com.medhead.model.GroupSpec;
import com.medhead.model.Hospital;
import com.medhead.model.Patient;
import com.medhead.model.Speciality;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class HospitalRepositoryTest {

    @Autowired
    private HospitalRepository hospitalRepository;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Autowired
    private GroupRepository groupRepository;

    // JUnit test for saveGroup
    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveGroupSpecTest(){

        GroupSpec groupSpec = GroupSpec.builder().groupName("Fetish").build();

        groupRepository.save(groupSpec);

        boolean expected = groupRepository.selectExistsName("Fetish");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for Speciality
    @Order(2)
    @Test
    @Rollback(value = false)
    public void saveSpecialityTest(){

        GroupSpec groupSpec = groupRepository.getById(1);

        Speciality speciality = Speciality.builder()
                .specName("See the sky")
                .group(groupSpec)
                .build();

        specialityRepository.save(speciality);

        boolean expected = specialityRepository.selectExistsName("See the sky");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for savePatient
    @Order(3)
    @Test
    @Rollback(value = false)
    public void saveHospitalTest(){

        List<Speciality> spec = specialityRepository.findAll();

        Hospital hospital= Hospital.builder()
                .name("Saint Clovis")
                .latitude(43.853458960795)
                .longitude(3.420394857384)
                .speciality(spec)
                .build();

        hospitalRepository.save(hospital);

        boolean expected = hospitalRepository.selectExistsName("Saint Clovis");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(4)
    public void getHospitalTest(){

        Hospital hospital = hospitalRepository.findById(1).get();

        Assertions.assertThat(hospital.getId()).isEqualTo(1);
    }


    @Test
    @Order(5)
    public void getListOfHospitalTest(){

        List<Hospital> hospital = hospitalRepository.findAll();

        Assertions.assertThat(hospital.size()).isGreaterThan(0);
    }

    @Test
    @Order(6)
    @Rollback(value = false)
    public void updateHospitalTest(){

        Hospital hospital = hospitalRepository.findById(1).get();

        hospital.setName("Saint Mathew");

        Hospital hospitalUpdated = hospitalRepository.save(hospital);

        Assertions.assertThat(hospitalUpdated.getName()).isEqualTo("Saint Mathew");
    }



    @Test
    @Order(7)
    @Rollback(value = false)
    public void deleteHospitalTest(){

        Hospital hospital = hospitalRepository.findById(1).get();

        hospitalRepository.delete(hospital);

        Hospital hospital1 = null;

        Optional<Hospital> optionalHospital = hospitalRepository.findByNewName("Saint Mathew");

        if (optionalHospital.isPresent()){
            hospital1 = optionalHospital.get();
        }

        Assertions.assertThat(hospital1).isNull();
    }

}