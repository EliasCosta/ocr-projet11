package com.medhead.repository;


import com.medhead.model.GroupSpec;
import com.medhead.model.Speciality;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SpecialityRepositoryTest {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private SpecialityRepository specialityRepository;

    // JUnit test for saveGroup
    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveGroupSpecTest(){

        GroupSpec groupSpec = GroupSpec.builder().groupName("Fetish").build();

        groupRepository.save(groupSpec);

        boolean expected = groupRepository.selectExistsName("Fetish");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for Speciality
    @Order(2)
    @Test
    @Rollback(value = false)
    public void saveSpecialityTest(){

        GroupSpec groupSpec = groupRepository.getById(1);

        Speciality speciality = Speciality.builder()
                .specName("See the sky")
                .group(groupSpec)
        .build();

        specialityRepository.save(speciality);

        boolean expected = specialityRepository.selectExistsName("See the sky");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(3)
    public void getSpecialityTest(){

        Speciality speciality = specialityRepository.findById(1).get();

        Assertions.assertThat(speciality.getId()).isEqualTo(1);
    }


    @Test
    @Order(4)
    public void getListOfSpecialityTest(){

        List<Speciality> speciality = specialityRepository.findAll();

        Assertions.assertThat(speciality.size()).isGreaterThan(0);
    }

    @Test
    @Order(5)
    @Rollback(value = false)
    public void updateSpecialityTest(){

        Speciality speciality = specialityRepository.findById(1).get();

        speciality.setSpecName("Dancing Hip-hop");

        Speciality specialityUpdated = specialityRepository.save(speciality);

        Assertions.assertThat(specialityUpdated.getSpecName()).isEqualTo("Dancing Hip-hop");
    }


    @Test
    @Order(6)
    @Rollback(value = false)
    public void deleteSpecialityTest(){

        Speciality speciality = specialityRepository.findById(1).get();

        specialityRepository.delete(speciality);

        Speciality speciality1 = null;

        Optional<Speciality> optionalSpeciality = specialityRepository.findByName("Dancing Hip-hop");

        if (optionalSpeciality.isPresent()){
            speciality1 = optionalSpeciality.get();
        }

        Assertions.assertThat(speciality1).isNull();
    }

}