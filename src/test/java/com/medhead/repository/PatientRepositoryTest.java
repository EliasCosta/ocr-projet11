package com.medhead.repository;



import com.medhead.model.GroupSpec;
import com.medhead.model.Patient;
import com.medhead.model.Speciality;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;

@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PatientRepositoryTest {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private SpecialityRepository specialityRepository;

    @Autowired
    private GroupRepository groupRepository;


    // JUnit test for saveGroup
    @Order(1)
    @Test
    @Rollback(value = false)
    public void saveGroupSpecTest(){

        GroupSpec groupSpec = GroupSpec.builder().groupName("Fetish").build();

        groupRepository.save(groupSpec);

        boolean expected = groupRepository.selectExistsName("Fetish");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for Speciality
    @Order(2)
    @Test
    @Rollback(value = false)
    public void saveSpecialityTest(){

        GroupSpec groupSpec = groupRepository.getById(1);

        Speciality speciality = Speciality.builder()
                .specName("See the sky")
                .group(groupSpec)
                .build();

        specialityRepository.save(speciality);

        boolean expected = specialityRepository.selectExistsName("See the sky");

        Assertions.assertThat(expected).isTrue();
    }

    // JUnit test for savePatient
    @Order(3)
    @Test
    @Rollback(value = false)
    public void savePatientTest(){

        List<Speciality> spec = specialityRepository.findAll();

        Patient patient = Patient.builder()
                .name("Anacleto Barros")
                .latitude(42.853458960795)
                .longitude(3.120394857384)
                .speciality(spec)
                .build();

        patientRepository.save(patient);

        boolean expected = patientRepository.selectExistsName("Anacleto Barros");

        Assertions.assertThat(expected).isTrue();
    }

    @Test
    @Order(4)
    public void getPatientTest(){

        Patient patient = patientRepository.findById(1).get();

        Assertions.assertThat(patient.getId()).isEqualTo(1);
    }


    @Test
    @Order(5)
    public void getListOfPatientTest(){

        List<Patient> patient = patientRepository.findAll();

        Assertions.assertThat(patient.size()).isGreaterThan(0);
    }


    @Test
    @Order(6)
    @Rollback(value = false)
    public void updatePatientTest(){

        Patient patient = patientRepository.findById(1).get();

        patient.setName("Miguel Tavares");

        Patient patientUpdated = patientRepository.save(patient);

        Assertions.assertThat(patientUpdated.getName()).isEqualTo("Miguel Tavares");
    }



    @Test
    @Order(7)
    @Rollback(value = false)
    public void deletePatientTest(){

        Patient patient = patientRepository.findById(1).get();

        patientRepository.delete(patient);

        Patient patient1 = null;

        Optional<Patient> optionalPatient = patientRepository.findByNewName("Miguel Tavares");

        if (optionalPatient.isPresent()){
            patient1 = optionalPatient.get();
        }

        Assertions.assertThat(patient1).isNull();
    }


}